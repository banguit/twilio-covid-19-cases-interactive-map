![Simple COVID-19 Cases Interactive Map Web Application](app-screenshot.png)
> [LIVE version](https://banguit.gitlab.io/twilio-covid-19-cases-interactive-map/)

# Simple COVID-19 Cases Interactive Map Web Application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) and based on standard Angular template.
In addition, I used additional libraries:
* [Angular Material Components](https://github.com/angular/components) for basic UX components.
* [NgRx](https://ngrx.io/) for state implementation.
* [Ignite UI](https://www.infragistics.com/products/ignite-ui-angular) Map component for Angular

## Description
This application is solution for Twilio Take Home Test. The app allows the end user to get statistics on COVID-19 cases from the UNStats API.
To get details about cases for specific country, user can click on map or search for country in the search field.
Because API not providing daily statistic and information about recovered patients are not available, I decided to add information about total confirmed cases across the world and top 10 countries table.

## Features
* Redux store implementation.
* Responsive UI for mobile/tablet devices.
* Interactive map. Display the select country data within the map component.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
