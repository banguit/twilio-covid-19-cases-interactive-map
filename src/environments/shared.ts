/**
 * @file Shared environment variables.
 * @author Dmytro Antonenko <dmitry.antonenko@hedgehog.com.ua>
 */
export const shared = {
    unstatsApiServiceUrl: 'https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/ncov_cases2_v1/FeatureServer/2/query?where=1%3D1&outFields=*&returnGeometry=false&outSR=4326&f=json',
    unstatsApiServiceByCountryUrl: 'https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/ncov_cases2_v1/FeatureServer/2/query?where=Country_Region%20%3D%20\'{Country_Region}\'&outFields=*&outSR=4326&f=json',
    worldDarkGrayMap: 'https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Dark_Gray_Base/MapServer',
}
