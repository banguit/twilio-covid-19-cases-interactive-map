/**
 * @file Production environment variables.
 * @author Dmytro Antonenko <dmitry.antonenko@hedgehog.com.ua>
 */
import {shared} from './shared';

export const environment = {
    ...shared,
    production: true
};
