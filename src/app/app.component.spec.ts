import {AngularMaterialModule} from './modules/angular-material/angular-material.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {provideMockStore, MockStore} from '@ngrx/store/testing';

describe('AppComponent', () => {
    let store: MockStore;
    let fixture: ComponentFixture<AppComponent>;
    const initialState = {};

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                AngularMaterialModule,
                BrowserAnimationsModule,
                RouterTestingModule,
            ],
            declarations: [
                AppComponent
            ],
            providers: [
                provideMockStore({initialState}),
            ]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        store = TestBed.inject(MockStore);
    });

    it('should create the app', () => {
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
    });
});
