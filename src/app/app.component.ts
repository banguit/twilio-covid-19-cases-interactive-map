import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {FetchCasesList} from './core/state/unstats/unstats.action';
import {MatSidenav} from '@angular/material/sidenav';
import {Store} from '@ngrx/store';
import {UnstatsState} from './core/state/unstats/unstats.reducer';
import {delay} from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
    @ViewChild(MatSidenav) sidenav!: MatSidenav;

    constructor(private observer: BreakpointObserver,
                private store: Store<UnstatsState>) {
        this.store.dispatch(new FetchCasesList());
    }

    ngAfterViewInit() {
        this.observer
        .observe(['(max-width: 800px)'])
        .pipe(delay(1))
        .subscribe((res) => {
            if (res.matches) {
                this.sidenav.mode = 'over';
                this.sidenav.close();
            } else {
                this.sidenav.mode = 'side';
                this.sidenav.open();
            }
        });
    }
}
