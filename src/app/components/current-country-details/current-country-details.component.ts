import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {UnstatsState} from '../../core/state/unstats/unstats.reducer';
import {getActiveCountry} from '../../core/state/unstats/unstats.selector';

@Component({
    selector: 'app-current-country-details',
    templateUrl: './current-country-details.component.html',
    styleUrls: ['./current-country-details.component.scss']
})
export class CurrentCountryDetailsComponent {
    selectedCountry$ = this.store.select(getActiveCountry);

    constructor(private store: Store<UnstatsState>) {
    }
}
