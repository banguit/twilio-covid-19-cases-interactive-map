import {ComponentFixture, TestBed} from '@angular/core/testing';
import {CurrentCountryDetailsComponent} from './current-country-details.component';
import {provideMockStore, MockStore} from '@ngrx/store/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('CurrentCountryDetailsComponent', () => {
    let component: CurrentCountryDetailsComponent;
    let store: MockStore;
    let fixture: ComponentFixture<CurrentCountryDetailsComponent>;
    const initialState = {};

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [CurrentCountryDetailsComponent],
            providers: [
                provideMockStore({initialState}),
            ]
        })
        .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CurrentCountryDetailsComponent);
        store = TestBed.inject(MockStore);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
