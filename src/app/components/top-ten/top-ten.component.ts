import {CasesCountry} from '../../core/models/cases-country';
import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {UnstatsState} from '../../core/state/unstats/unstats.reducer';
import {getCasesList} from '../../core/state/unstats/unstats.selector';

@Component({
    selector: 'app-top-ten',
    templateUrl: './top-ten.component.html',
    styleUrls: ['./top-ten.component.scss']
})
export class TopTenComponent implements OnInit {
    records: CasesCountry[] = [];
    displayedColumns: string[] = ['countryRegion', 'confirmed'];
    total: number = 0;

    constructor(private store: Store<UnstatsState>) {
    }

    ngOnInit(): void {
        this.store.select(getCasesList)
        .subscribe((cases) => {
            if (cases.length) {
                cases.sort((a, b) => b.confirmed - a.confirmed);

                this.records = cases.slice(0, 10);
                this.total =
                    cases.reduce((prev, current) => {
                        return current.confirmed + prev;
                    }, 0);
            }
        });
    }
}
