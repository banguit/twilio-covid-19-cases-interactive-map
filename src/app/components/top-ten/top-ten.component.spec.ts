import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TopTenComponent} from './top-ten.component';
import {provideMockStore, MockStore} from '@ngrx/store/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('TopTenComponent', () => {
    let component: TopTenComponent;
    let store: MockStore;
    let fixture: ComponentFixture<TopTenComponent>;
    const initialState = {};

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [TopTenComponent],
            providers: [
                provideMockStore({initialState}),
            ]
        })
        .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TopTenComponent);
        store = TestBed.inject(MockStore);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
