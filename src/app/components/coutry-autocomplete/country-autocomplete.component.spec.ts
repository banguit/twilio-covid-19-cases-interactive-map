import {AngularMaterialModule} from '../../modules/angular-material/angular-material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {CountryAutocompleteComponent} from './country-autocomplete.component';
import {provideMockStore, MockStore} from '@ngrx/store/testing';

describe('CountryAutocompleteComponent', () => {
    let component: CountryAutocompleteComponent;
    let store: MockStore;
    let fixture: ComponentFixture<CountryAutocompleteComponent>;
    const initialState = {};

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                AngularMaterialModule,
                BrowserAnimationsModule
            ],
            declarations: [CountryAutocompleteComponent],
            providers: [
                provideMockStore({initialState}),
            ]
        })
        .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CountryAutocompleteComponent);
        store = TestBed.inject(MockStore);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
