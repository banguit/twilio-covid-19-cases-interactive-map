import {CasesCountry} from '../../core/models/cases-country';
import {Component, OnInit, ViewChild} from '@angular/core';
import {FetchCases} from '../../core/state/unstats/unstats.action';
import {FormControl} from '@angular/forms';
import {MatAutocomplete} from '@angular/material/autocomplete';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {UnstatsState} from '../../core/state/unstats/unstats.reducer';
import {getCasesList, getError} from '../../core/state/unstats/unstats.selector';
import {map, startWith} from 'rxjs/operators';

@Component({
    selector: 'app-country-autocomplete',
    templateUrl: './country-autocomplete.component.html',
    styleUrls: ['./country-autocomplete.component.scss']
})
export class CountryAutocompleteComponent implements OnInit {
    options: CasesCountry[] = [];
    searchControl = new FormControl();
    filteredOptions: Observable<CasesCountry[]> = this.searchControl.valueChanges
        .pipe(
            startWith(''),
            map(value => typeof value === 'string' ? value : value.countryRegion),
            map(name => name ? this.filter(name) : this.options.slice())
        );
    @ViewChild('auto', {static: true}) autocomplete!: MatAutocomplete;

    constructor(private store: Store<UnstatsState>,
                private snackBar: MatSnackBar) {
    }

    ngOnInit(): void {
        this.store.select(getCasesList)
            .subscribe((cases) => {
                this.options = cases;
            });

        this.store.select(getError)
            .subscribe((error) => {
                if (error?.length) {
                    this.snackBar.open(error, 'Close', {
                        verticalPosition: 'top',
                        panelClass: 'warn-snackbar'
                    });
                }
            });
    }

    displayFn(casesCountry: CasesCountry): string {
        return casesCountry && casesCountry.countryRegion ? casesCountry.countryRegion : '';
    }

    onSearchButtonClick($event: Event) {
        const value = this.searchControl.value;

        if (value) {
            const countryName = typeof value === 'string' ? value : value.countryRegion;
            const countryCases = this.options.find((value) =>
                value.countryRegion.toLowerCase() === countryName.toLowerCase());

            if (countryCases) {
                this.store.dispatch(new FetchCases({countryRegion: countryCases.countryRegion}));
            } else {
                this.snackBar.open('Invalid country name', 'Close', {
                    duration: 40000,
                    verticalPosition: 'top',
                    panelClass: 'warn-snackbar'
                });
            }
        }
        $event.stopPropagation();
    }

    private filter(name: string): CasesCountry[] {
        const filterValue = name.toLowerCase();
        return this.options.filter(option => option.countryRegion.toLowerCase().includes(filterValue));
    }
}
