import {ComponentFixture, TestBed} from '@angular/core/testing';
import {InteractiveMapComponent} from './interactive-map.component';
import {provideMockStore, MockStore} from '@ngrx/store/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {IgxDataChartInteractivityModule} from 'igniteui-angular-charts';
import {IgxGeographicMapCoreModule, IgxGeographicMapModule} from 'igniteui-angular-maps';

describe('InteractiveMapComponent', () => {
    let component: InteractiveMapComponent;
    let store: MockStore;
    let fixture: ComponentFixture<InteractiveMapComponent>;
    const initialState = {};

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [InteractiveMapComponent],
            providers: [
                provideMockStore({initialState}),
            ],
            imports: [
                IgxDataChartInteractivityModule,
                IgxGeographicMapCoreModule,
                IgxGeographicMapModule,
            ]
        })
        .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(InteractiveMapComponent);
        store = TestBed.inject(MockStore);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
