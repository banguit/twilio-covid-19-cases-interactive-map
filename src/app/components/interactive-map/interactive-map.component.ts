import {ArcGISOnlineMapImagery, IgxGeographicMapComponent, IgxGeographicProportionalSymbolSeriesComponent} from 'igniteui-angular-maps';
import {CasesCountry} from '../../core/models/cases-country';
import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FetchCases} from '../../core/state/unstats/unstats.action';
import {IgxSizeScaleComponent, IgxValueBrushScaleComponent, MarkerType} from 'igniteui-angular-charts';
import {Rect} from 'igniteui-angular-core';
import {Store} from '@ngrx/store';
import {UnstatsState} from '../../core/state/unstats/unstats.reducer';
import {environment} from '../../../environments/environment';
import {getActiveCountry, getCasesList} from '../../core/state/unstats/unstats.selector';

@Component({
    selector: 'app-interactive-map',
    templateUrl: './interactive-map.component.html',
    styleUrls: ['./interactive-map.component.scss']
})
export class InteractiveMapComponent implements OnInit {
    @ViewChild('map', {static: true}) map!: IgxGeographicMapComponent;
    @ViewChild('tooltip', {static: true}) tooltip!: TemplateRef<object>;

    constructor(private store: Store<UnstatsState>) {
    }

    ngOnInit(): void {
        this.setDarkMap();
        this.setInitialMapZoom();
        this.store.select(getCasesList)
            .subscribe((cases) => {
                if (cases.length) {
                    this.addMapSeries(JSON.parse(JSON.stringify(cases)));
                }
            });

        this.store.select(getActiveCountry).subscribe((country: CasesCountry) => {
            if (country) {
                this.zoomMapToLocation(country.latitude, country.longitude);
            }
        })
    }

    /**
     * Sets dark map canvas.
     * @private
     */
    private setDarkMap(): void {
        const tileSource = new ArcGISOnlineMapImagery();
        tileSource.mapServerUri = InteractiveMapComponent.getMapServerUri();
        (tileSource as any).i = tileSource;
        (this.map as any).backgroundContent = tileSource;
    }

    /**
     * Initialize map in the middle of the component.
     * @private
     */
    private setInitialMapZoom() {
        const geoBounds = {
            height: 0,
            left: -0,
            top: 40,
            width: 260
        };
        this.map.zoomToGeographic(geoBounds);
    }

    /**
     * Fill the map series.
     */
    private addMapSeries(cases: CasesCountry[]) {
        const locations = cases;
        const symbolSeries = this.createSeries();
        const sizeScale = this.createSizeScale();
        const brushScale = this.createBrushScale();

        symbolSeries.dataSource = locations;
        symbolSeries.radiusScale = sizeScale;
        symbolSeries.fillScale = brushScale;
        symbolSeries.markerOutline = brushScale.brushes[0];
        symbolSeries.tooltipTemplate = this.tooltip;

        this.map.series.clear();
        this.map.series.add(symbolSeries);
    }

    private createSeries(): IgxGeographicProportionalSymbolSeriesComponent {
        const symbolSeries = new IgxGeographicProportionalSymbolSeriesComponent();
        symbolSeries.markerType = MarkerType.Circle;
        symbolSeries.fillMemberPath = 'deaths';
        symbolSeries.radiusMemberPath = 'confirmed';
        symbolSeries.latitudeMemberPath = 'latitude';
        symbolSeries.longitudeMemberPath = 'longitude';
        return symbolSeries;
    }

    private createSizeScale(): IgxSizeScaleComponent {
        const sizeScale = new IgxSizeScaleComponent();
        sizeScale.minimumValue = 3;
        sizeScale.maximumValue = 50;
        sizeScale.isLogarithmic = true;
        return sizeScale;
    }

    private createBrushScale(): IgxValueBrushScaleComponent {
        const brushScale = new IgxValueBrushScaleComponent();
        brushScale.brushes = [
            'rgba(0,153,255, .3)',  // semi-transparent green
            'rgba(95,191,112, .4)', // semi-transparent orange
            'rgba(255, 138, 144, .4)',  // semi-transparent red
        ];
        brushScale.minimumValue = 1;
        brushScale.maximumValue = 1000000;
        return brushScale;
    }

    /**
     * Series click event handler.
     */
    public onSeriesClicked(event: any) {
        this.store.dispatch(new FetchCases({countryRegion: event.args.item.countryRegion}));
        this.zoomMapToLocation(event.args.item.latitude, event.args.item.longitude);
    }

    /**
     * Zoom the map to show the corresponding country.
     */
    private zoomMapToLocation(latitude: number, longitude: number) {
        const geoRect = new Rect(0, longitude - 5, latitude - 8, 10, 15);
        this.map.zoomToGeographic(geoRect);
    }

    /**
     * Resolving uri based on hosting website.
     */
    private static getMapServerUri(): string {
        const isHttpSecured = window.location.toString().startsWith('https:');
        let uri = environment.worldDarkGrayMap;

        if (!isHttpSecured) {
            uri = uri.replace('https:', 'http:');
        }

        return uri;
    }
}
