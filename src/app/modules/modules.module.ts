/**
 * Module for all application modules.
 * Has to be imported by root module.
 */
import {AngularMaterialModule} from './angular-material/angular-material.module';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

const modules = [
    AngularMaterialModule,
];

@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,
        ...modules,
    ],
    exports: [
        ...modules,
    ],
})
export class ModulesModule {
}
