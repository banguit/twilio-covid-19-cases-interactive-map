/**
 * Application root module.
 */
import {AppComponent} from './app.component';
import {AppServicesModule} from './app-services.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CountryAutocompleteComponent} from './components/coutry-autocomplete/country-autocomplete.component';
import {CurrentCountryDetailsComponent} from './components/current-country-details/current-country-details.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {IgxDataChartInteractivityModule} from 'igniteui-angular-charts';
import {IgxGeographicMapCoreModule, IgxGeographicMapModule} from 'igniteui-angular-maps';
import {InteractiveMapComponent} from './components/interactive-map/interactive-map.component';
import {ModulesModule} from './modules/modules.module';
import {StateModule} from './core/state/state.module';
import {TopTenComponent} from './components/top-ten/top-ten.component';

@NgModule({
    declarations: [
        AppComponent,
        CountryAutocompleteComponent,
        CurrentCountryDetailsComponent,
        InteractiveMapComponent,
        TopTenComponent,
    ],
    imports: [
        AppServicesModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        IgxDataChartInteractivityModule,
        IgxGeographicMapCoreModule,
        IgxGeographicMapModule,
        ModulesModule,
        ReactiveFormsModule,
        StateModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
