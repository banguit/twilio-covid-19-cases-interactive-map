/**
 * Module for application services. Provided services as singleton
 * throughout the application. Has to be imported by root module.
 */
import {NgModule} from '@angular/core';
import {UnstatsService} from './core/services/unstats/unstats.service';

@NgModule({
    providers: [
        UnstatsService
    ],
})
export class AppServicesModule {
}
