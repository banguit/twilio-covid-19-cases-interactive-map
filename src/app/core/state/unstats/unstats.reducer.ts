/**
 * @file UNStats feature NgRx reducer.
 * @author Dmytro Antonenko <dmitry.antonenko@hedgehog.com.ua>
 */
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {CasesCountry} from '../../models/cases-country';
import {UnstatsActions, UnstatsActionType} from './unstats.action';

/**
 * UNStats state feature name.
 */
export const UNSTATS_FEATURE = 'UNStats';

/**
 * UNStats entity state.
 */
export interface UnstatsState extends EntityState<CasesCountry> {
    activeCountryUid: number;
    error: any;
    loaded: boolean;
}

/**
 * Entity adapter foa a single entity state collection.
 */
export const unstatsAdapter: EntityAdapter<CasesCountry> = createEntityAdapter<CasesCountry>({
    selectId: model => model.uid,
});

/**
 * Initial UNStats state.
 */
export const initialState: UnstatsState = unstatsAdapter.getInitialState({
    activeCountryUid: -1,
    error: null,
    loaded: false,
});

export function unstatsReducer(state: UnstatsState = initialState, action: UnstatsActions): UnstatsState {
    switch (action.type) {
        case UnstatsActionType.FetchCases:
        case UnstatsActionType.FetchCasesList:
            return {...state, loaded: false};

        case UnstatsActionType.FetchCasesListSuccess:
            return unstatsAdapter.addMany(action.payload.cases, {...state, loaded: true, activeCountryUid: -1});

        case UnstatsActionType.FetchCasesSuccess:
            return unstatsAdapter.setOne(action.payload.cases, {...state, activeCountryUid: action.payload.cases.uid, loaded: true});

        case UnstatsActionType.FetchCasesListError:
            return {...state, error: action.payload.error};

        default:
            return state;
    }
}
