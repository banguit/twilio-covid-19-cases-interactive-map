/**
 * @file UNstats feature NgRx module.
 * @author Dmytro Antonenko <dmitry.antonenko@hedgehog.com.ua>
 */
import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {UNSTATS_FEATURE, unstatsReducer} from './unstats.reducer';
import {EffectsModule} from '@ngrx/effects';
import {UnstatsEffects} from './unstats.effects';

@NgModule({
    imports: [
        StoreModule.forFeature(UNSTATS_FEATURE, unstatsReducer),
        EffectsModule.forFeature([UnstatsEffects]),
    ]
})
export class UnstatsStateModule {}
