/**
 * @file UNStats feature NgRx effects.
 * @author Dmytro Antonenko <dmitry.antonenko@hedgehog.com.ua>
 */
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {CasesCountry} from '../../models/cases-country';
import {FetchCases, FetchCasesError, FetchCasesListError, FetchCasesListSuccess, FetchCasesSuccess, UnstatsActionType} from './unstats.action';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {UnstatsService} from '../../services/unstats/unstats.service';
import {UnstatsState} from './unstats.reducer';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable({providedIn: 'root'})
export class UnstatsEffects {
    fetchCasesList$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UnstatsActionType.FetchCasesList),
            mergeMap(() => this.unstatsService.getCasesList()),
            map((cases: CasesCountry[]) => new FetchCasesListSuccess({cases})),
            catchError((error) => of(new FetchCasesListError({error})))
        )
    });

    fetchCases$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UnstatsActionType.FetchCases),
            mergeMap((action: FetchCases) => this.unstatsService.getCases(action.payload.countryRegion)),
            map((cases: CasesCountry|null) => cases ? new FetchCasesSuccess({cases}) : new FetchCasesError({error: `Record not found`})),
            catchError((error) => of(new FetchCasesError({error})))
        );
    });

    constructor(
        private actions$: Actions,
        private unstatsService: UnstatsService,
        private store: Store<UnstatsState>) {
    }
}
