/**
 * @file UNstats feature NgRx actions.
 * @author Dmytro Antonenko <dmitry.antonenko@hedgehog.com.ua>
 */
import {Action} from '@ngrx/store';
import {CasesCountry} from '../../models/cases-country';

export enum UnstatsActionType {
    FetchCasesList = '[Unstats] CasesList',
    FetchCasesListSuccess = '[Unstats] FetchCasesListSuccess',
    FetchCasesListError = '[Unstats] FetchCasesListError',
    FetchCases = '[Unstats] FetchCases',
    FetchCasesSuccess = '[Unstats] FetchCasesSuccess',
    FetchCasesError = '[Unstats] FetchCasesError',
}

export class FetchCasesList implements Action {
    readonly type = UnstatsActionType.FetchCasesList;
}

export class FetchCasesListSuccess implements Action {
    readonly type = UnstatsActionType.FetchCasesListSuccess;
    constructor(public payload: {cases: CasesCountry[]}) {}
}

export class FetchCasesListError implements Action {
    readonly type = UnstatsActionType.FetchCasesListError;
    constructor(public payload: {error: string}) {}
}

export class FetchCases implements Action {
    readonly type = UnstatsActionType.FetchCases;
    constructor(public payload: {countryRegion: string}) {}
}

export class FetchCasesSuccess implements Action {
    readonly type = UnstatsActionType.FetchCasesSuccess;
    constructor(public payload: {cases: CasesCountry}) {}
}

export class FetchCasesError implements Action {
    readonly type = UnstatsActionType.FetchCasesError;
    constructor(public payload: {error: string}) {}
}

export type UnstatsActions =
    FetchCasesList |
    FetchCasesListSuccess |
    FetchCasesListError |
    FetchCases |
    FetchCasesSuccess |
    FetchCasesError;
