/**
 * @file UNStats feature NgRx selectors.
 * @author Dmytro Antonenko <dmitry.antonenko@hedgehog.com.ua>
 */
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {UNSTATS_FEATURE, unstatsAdapter, UnstatsState} from './unstats.reducer';
import {CasesCountry} from '../../models/cases-country';

/**
 * UNStats top-level feature state.
 */
export const getUnstatsState = createFeatureSelector<UnstatsState>(UNSTATS_FEATURE);

/**
 * Is UNStats data loaded.
 */
export const isLoaded = createSelector(getUnstatsState, (state: UnstatsState) => state?.loaded);

/**
 * Gets all UNStats cases.
 */
export const getCasesList = createSelector(getUnstatsState, unstatsAdapter.getSelectors().selectAll);

/**
 * Gets error.
 */
export const getError = createSelector(getUnstatsState, (state: UnstatsState) => state?.error);

/**
 * Gets active country.
 */
export const getActiveCountry = createSelector(getUnstatsState,
    (state: UnstatsState) => state.entities[state.activeCountryUid] as CasesCountry);
