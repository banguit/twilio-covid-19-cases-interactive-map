/**
 * Root state configuration.
 */
import {RuntimeChecks} from '@ngrx/store/src/models';
import {environment} from '../../../environments/environment';

/**
 * Root store configurations.
 */
const checkSetting = !environment.production;
export const rootStoreConfig: RuntimeChecks = {
    strictStateImmutability: checkSetting,
    strictActionImmutability: checkSetting,
    strictStateSerializability: false,
    strictActionSerializability: false,
    strictActionWithinNgZone: checkSetting,
    strictActionTypeUniqueness: checkSetting
};
