/**
 * Application state root module. Has to be imported by root module.
 */
import {EffectsModule} from '@ngrx/effects';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {StoreModule} from '@ngrx/store';
import {UnstatsStateModule} from './unstats/unstats.module';
import {environment} from '../../../environments/environment';
import {rootStoreConfig} from './root-store.config';

const features = [
    UnstatsStateModule,
];

@NgModule({
    imports: [
        ...features,
        StoreModule.forRoot({}, {runtimeChecks: rootStoreConfig}),
        EffectsModule.forRoot([]),
        HttpClientModule,
        !environment.production ? StoreDevtoolsModule.instrument() : [],
    ]
})
export class StateModule {
}
