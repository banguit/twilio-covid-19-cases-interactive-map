import {TestBed} from '@angular/core/testing';
import {UnstatsService} from './unstats.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('UnstatsService', () => {
    let service: UnstatsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ]
        });
        service = TestBed.inject(UnstatsService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
