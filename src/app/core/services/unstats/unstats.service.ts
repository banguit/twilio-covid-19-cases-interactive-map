/**
 * @file UNStats COVID-19 response service.
 * @author Dmytro Antonenko <dmitry.antonenko@hedgehog.com.ua>
 */
import {CasesCountry} from '../../models/cases-country';
import {Feature, UnstatsCasesCountryResponse} from '../../models/unstats-cases-country-response';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';

@Injectable({providedIn: 'root'})
export class UnstatsService {
    constructor(private http: HttpClient) {
    }

    /**
     * Gets all cases by country.
     */
    public getCasesList(): Observable<CasesCountry[]> {
        return this.http.get<UnstatsCasesCountryResponse>(environment.unstatsApiServiceUrl)
            .pipe(
                map((response: UnstatsCasesCountryResponse) =>
                    response.features.map((feature) => this.featureToCasesCountry(feature))
                ),
                catchError(this.httpErrorHandler));
    }

    /**
     * Get cases for country.
     * @param countryRegion
     */
    public getCases(countryRegion: string): Observable<CasesCountry|null> {
        const url = environment.unstatsApiServiceByCountryUrl.replace('{Country_Region}', countryRegion);
        return this.http.get<UnstatsCasesCountryResponse>(url)
            .pipe(
                map((response: UnstatsCasesCountryResponse) => {
                    const feature = response.features[0];
                    if (feature === undefined) { return null; }
                    return this.featureToCasesCountry(feature);
                }),
                catchError(this.httpErrorHandler));
    }

    private featureToCasesCountry(feature: Feature) {
        return <CasesCountry>{
            latitude: feature.attributes.Lat,
            confirmed: feature.attributes.Confirmed,
            countryRegion: feature.attributes.Country_Region,
            deaths: feature.attributes.Deaths,
            incidentRate: feature.attributes.Incident_Rate,
            iso3: feature.attributes.ISO3,
            uid: feature.attributes.UID,
            longitude: feature.attributes.Long_,
            mortalityRate: feature.attributes.Mortality_Rate,
            recovered: feature.attributes.Recovered,
            lastUpdate: feature.attributes.Last_Update,
        };
    }

    /**
     * Http error handler.
     */
    private httpErrorHandler(error: any) {
        let errorMsg: string;
        if (error.error instanceof ErrorEvent) {
            errorMsg = `Error: ${error.error.message}`;
        } else {
            errorMsg = this.getServerErrorMessage(error);
        }
        return throwError(errorMsg);
    }

    /**
     * Gets server error message.
     */
    private getServerErrorMessage(error: HttpErrorResponse): string {
        switch (error.status) {
            case 400:
                return `Bad request: ${error.message}`;
            case 404:
                return `Not Found: ${error.message}`;
            case 500:
                return `Internal Server Error: ${error.message}`;
            default:
                return error.message;
        }
    }
}
