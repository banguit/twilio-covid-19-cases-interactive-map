/**
 * UNStats cases country response model.
 */
export interface UnstatsCasesCountryResponse {
    features: Feature[];
}

export interface Feature {
    attributes: Attributes;
}

interface Attributes {
    Confirmed: number;
    Country_Region: string;
    Deaths: number;
    ISO3: string;
    Incident_Rate: number;
    Last_Update: number;
    Lat: number;
    Long_: number;
    Mortality_Rate: number;
    OBJECTID: number;
    People_Hospitalized: number;
    People_Tested: number;
    Recovered: number;
    UID: number;
}
