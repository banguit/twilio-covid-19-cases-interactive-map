/**
 * Cases country model.
 */
export interface CasesCountry {
    confirmed: number;
    countryRegion: string;
    deaths: number;
    incidentRate: number;
    iso3: string;
    lastUpdate: number;
    latitude: number;
    longitude: number;
    mortalityRate: number;
    recovered: number;
    uid: number;
}
